﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace ProceduralNodeEditor
{
    [Serializable]
    public class NodeMap : ScriptableObject, IEnumerable<Node>
    {
        [SerializeField]
        private Node current;
        public List<Node> nodes;
        public List<Node> Nodes { get => nodes; }
        public Node Current
        {
            get
            {
                return current;
            }
            set
            {
                current = value;
                Selection.activeObject = this;
            }
        }
        public NodeMap()
        {
            nodes = new List<Node>();
        }
        public virtual void Draw()
        {
            foreach(Node node in nodes)
                node.Draw();
        }
        public virtual bool HandleEvents(Event current)
        {
            if (nodes == null) nodes = new List<Node>();
            return nodes.Any(n => n.HandleEvents(current));
        }
        public void CreateNode(Vector2 position, Type type)
        {
            Node node = Node.Create(position, nodes.Count, type);
            nodes.Add(node);
            Debug.Log(node.ToString());
            Debug.Log(nodes.Count);
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return ((IEnumerable<Node>)nodes).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Node>)nodes).GetEnumerator();
        }
    }
}
