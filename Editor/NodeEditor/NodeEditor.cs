﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using System.Linq;

namespace ProceduralNodeEditor
{
    public class NodeEditor : EditorWindow
    {
        internal static NodeEditor editor;
        [MenuItem("NodeEditor/Editor")]
        internal static void Init()
        {
            editor = GetWindow<NodeEditor>();
            editor.CheckNulls(true);
        }
        internal NodeAttribute[] baseNodes;
        private NodeMap map;
        public static Dictionary<string, GUIStyle> editorStyles;
        public void OnGUI()
        {
            CheckNulls();
            if (HandleEvents(Event.current) || recalculate)
            {
                recalculate = false;
            }
            map.Draw();
        }
        private bool recalculate = true;
        public bool HandleEvents(Event current)
        {
            if (map.HandleEvents(current))
            {
                return true;
            }
            else
                switch (current.type)
                {
                    case EventType.MouseDown:
                        if(current.button == 0)
                        {

                        } else
                        {
                            ShowMenu(current.mousePosition);
                        }
                        break;
                }
                return false;
        }
        private void ShowMenu(Vector2 position)
        {
            GenericMenu menu = new GenericMenu();
            foreach(NodeAttribute nodeAttribute in baseNodes)
            {
                menu.AddItem(new GUIContent(nodeAttribute.contextPath), false, () => { CreateNode(position, nodeAttribute.type); });
            }
            menu.ShowAsContext();
        }
        private void CreateNode(Vector2 position, Type type)
        {
            recalculate = true;
            map.CreateNode(position, type);
        }
        private void CheckNulls(bool force = false)
        {
            if (editorStyles == null || force) editorStyles = LoadTextures();
            if (baseNodes == null || force) baseNodes = BuildBaseNodeList();
            if (map == null || force) map = new NodeMap();
        }
        private NodeAttribute[] BuildBaseNodeList()
        {
            return AppDomain.CurrentDomain.GetAssemblies().Where(asm => !asm.GlobalAssemblyCache).SelectMany(x => x.GetTypes()).Where(y => y.GetCustomAttribute<NodeAttribute>() != null).Select(z => z.GetCustomAttribute<NodeAttribute>()).ToArray();    
        }
        private Dictionary<string, GUIStyle> LoadTextures()
        {
            Dictionary<string, GUIStyle> result = new Dictionary<string, GUIStyle>();
            GUIStyle nodeStyle = new GUIStyle();
            nodeStyle.border = new RectOffset(12, 12, 12, 12);
            nodeStyle.normal.background = (Texture2D)EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png");
            result.Add("Node", nodeStyle);
            return result;
        }
    }
}

