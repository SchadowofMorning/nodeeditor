﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProceduralNodeEditor
{
    [Serializable]
    public class Node : ScriptableObject
    {
        protected int id;
        [SerializeField]
        protected Handle[] handles;
        public int ID { get => id; }
        public virtual void Calculate() { }
        public Node()
        {
            
        }
        public static Node Create(int id, Type type)
        {
            Node node = (Node)ScriptableObject.CreateInstance(type);
            node.id = id;
            return node;
        }
        public bool CollectInputs()
        {

            return true;
        }
#if UNITY_EDITOR
        private Vector2 position;
        protected virtual Rect Rect
        {
            get
            {
                return new Rect(position, new Vector2(50, 50));
            }
        }
        private bool changed;
        public Vector2 Position { get => position; }
        public bool Changed { get => changed; }
        private bool dragging = false;
        public void Drag(Vector2 delta)
        {
            position += delta;
            changed = true;
        }
        public static Node Create(Vector2 position, int id, Type type)
        {
            Node node = Create(id, type);
            node.position = position;
            node.changed = true;
            node.handles = node.BuildHandles();
            return node;
        }
        private Handle[] BuildHandles()
        {
            List<Handle> handles = new List<Handle>();
            foreach (FieldInfo fieldInfo in this.GetType().GetFields()
                .Where(fieldInfo => fieldInfo.GetCustomAttribute<HandleAttribute>() != null))
            {
                HandleAttribute handleAttribute = fieldInfo.GetCustomAttribute<HandleAttribute>();
                Handle handle = new Handle(this, fieldInfo.Name, handleAttribute.type, handleAttribute.direction, handleAttribute.required);
                handles.Add(handle);
            }
            return handles.ToArray();
        }
        public virtual bool HandleEvents(Event current)
        {
            switch (current.type)
            {
                case EventType.MouseDown:
                    if (Rect.Contains(current.mousePosition))
                    {
                        if(current.button == 0)
                        {
                            Selection.activeObject = this;
                            dragging = true;
                        }
                        return true;
                    }
                    return false;
                case EventType.MouseDrag:
                    if (dragging)
                    {
                        Drag(current.delta);
                        current.Use();
                        return true;
                    }
                    return false;
                case EventType.MouseUp:
                    if (dragging == true)
                    {
                        dragging = false;
                        return true;
                    }
                    return false;
                default: return false;
            }
        }
        public virtual void Draw()
        {
            GUI.Box(Rect, new GUIContent(), NodeEditor.editorStyles["Node"]);
            DrawHandles();
        }
        public virtual void DrawHandles()
        {
            int inLength = 0;
            int outLength = 0;
            for (int i = 0; i < handles.Length; i++)
            {
                Handle handle = handles[i];
                if(handle.direction == HandleDirection.IN)
                {
                    handle.Draw(Rect, inLength);
                    inLength++;
                } else
                {
                    handle.Draw(Rect, outLength);
                    outLength++;
                }
            }
        }
#endif
    }
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class NodeAttribute : Attribute
    {
        public readonly string identifier;
        public readonly string contextPath;
        public readonly Type type;
        public readonly Type baseType;
        public NodeAttribute(string identifier, string contextPath, Type type, Type baseType)
        {
            this.identifier = identifier;
            this.contextPath = contextPath;
            this.type = type;
            this.baseType = baseType;
        }
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", identifier, contextPath, type, baseType);
        }
    }
}
