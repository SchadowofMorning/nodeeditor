﻿using System;
using System.Reflection;
using UnityEngine;

namespace ProceduralNodeEditor
{
    [Serializable]
    public class Handle
    {
        public HandleDirection direction;
        public Node parent;
        public string field;
        public HandleConnection connection;
        public Type type;
        public bool required;
        public object Value
        {
            get
            {
                if(direction == HandleDirection.IN)
                {
                    FieldInfo fieldInfo = parent.GetType().GetField(field);
                    return fieldInfo.GetValue(parent);
                } 
                {
                    if (connection.origin == null) return null;
                    else
                    {
                        return connection.origin.Value;
                    }
                }
            }
        }
        public Handle()
        {

        }
        public Handle(Node parent, string field, Type type, HandleDirection direction, bool required)
        {
            this.parent = parent;
            this.field = field;
            this.type = type;
            this.direction = direction;
            this.required = required;
        }
#if UNITY_EDITOR
        public void Draw(Rect parentRect, int index)
        {
            const int width = 50;
            const int height = 20;
            Vector2  position = direction == HandleDirection.IN ? new Vector2(parentRect.xMin - width, parentRect.yMin + (index * height)) : new Vector2(parentRect.xMax, parentRect.yMin + (index * height));
            Vector2 size = new Vector2(width, height);
            string text = string.Format("{0} >", field);
            GUI.Box(new Rect(position, size), new GUIContent(text));
        }
#endif
    }
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class HandleAttribute : Attribute
    {
        public readonly HandleDirection direction;
        public readonly Type type;
        public readonly bool required;
        public HandleAttribute(HandleDirection direction, Type type, bool required)
        {
            this.direction = direction;
            this.type = type;
            this.required = required;
        }
    }
    [Serializable]
    public enum HandleDirection
    {
        IN,
        OUT
    }
}
