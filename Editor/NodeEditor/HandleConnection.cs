﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProceduralNodeEditor
{
    [Serializable]
    public class HandleConnection
    {
        public Handle origin;
        public Handle target;
        public HandleConnection()
        {

        }
        public HandleConnection(Handle origin, Handle target)
        {
            this.origin = origin;
            this.target = target;
        }
    }
}
