﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProceduralNodeEditor.General
{
    [Node("PerlinNoiseNode", "Matrix/Noise/Perlin", typeof(PerlinNoiseNode), typeof(MatrixNode)), Serializable]
    public class PerlinNoiseNode : Node
    {
        [Handle(HandleDirection.IN, typeof(int), false)]
        public int size;
        [Handle(HandleDirection.OUT, typeof(float[,]), true)]
        public float[,] data;
        public override void Calculate()
        {
            
        }
    }
}
