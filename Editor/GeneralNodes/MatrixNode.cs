﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ProceduralNodeEditor.General
{
    public abstract class MatrixNode : Node
    {
        [Handle(HandleDirection.IN, typeof(float[,]), true)]
        public float[,] input;
        [Handle(HandleDirection.OUT, typeof(float[,]), true)]
        public float[,] output;

        protected Texture2D renderTexture;
        public override void Calculate()
        {
            input = output;
        }
    }
}
